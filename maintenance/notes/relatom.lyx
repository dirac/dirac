#LyX 1.4.3 created this file. For more info see http://www.lyx.org/
\lyxformat 245
\begin_document
\begin_header
\textclass article
\begin_preamble
\usepackage{a4wide}
\date{}
\end_preamble
\language english
\inputencoding auto
\fontscheme default
\graphics default
\paperfontsize default
\spacing single
\papersize a4paper
\use_geometry false
\use_amsmath 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\end_header

\begin_body

\begin_layout Title

\series bold
Modified solid harmonics
\end_layout

\begin_layout Author

\size small
Notes started by:
\size default

\newline
T.
 Saue, S.
 Reine and L.
 Visscher
\end_layout

\begin_layout Subsection*
Atoms: the radial Dirac equation
\end_layout

\begin_layout Standard
The atomic Dirac equation is
\begin_inset Formula \begin{equation}
\left[\begin{array}{cc}
V & c\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right)\\
c\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right) & V-2mc^{2}\end{array}\right]\left[\begin{array}{c}
\psi^{L}\\
\psi^{S}\end{array}\right]=\left[\begin{array}{c}
\psi^{L}\\
\psi^{S}\end{array}\right]E\label{eq:Direq}\end{equation}

\end_inset


\end_layout

\begin_layout Standard
where the interaction 
\begin_inset Formula $V$
\end_inset

 has spherical symmetry.
 The orbitals have the general form
\begin_inset Formula \begin{equation}
\psi=\left[\begin{array}{c}
R^{L}(r)\chi_{\kappa,m}(\theta,\phi)\\
iR^{S}(r)\chi_{-\kappa,m}(\theta,\phi)\end{array}\right]\label{eq:relAO}\end{equation}

\end_inset

 where the 2-component angular functions for the large and the small components
 are related through
\begin_inset Formula \begin{equation}
\chi_{\kappa,m}=\frac{1}{r}\left(\boldsymbol{\sigma}\cdot\mathbf{r}\right)\chi_{-\kappa,m}\label{eq:2ang}\end{equation}

\end_inset

 Separation of variables is obtained by writing out the system of equations
\begin_inset Formula \[
\begin{array}{lclcl}
V\psi^{L} & + & c\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right)\psi^{S} & = & E\psi^{L}\\
c\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right)\psi^{L} & + & \left(V-2mc^{2}\right)\psi^{S} & = & E\psi^{S}\end{array}\]

\end_inset

 and the premultiplying with 
\begin_inset Formula $\frac{1}{r}\left(\boldsymbol{\sigma}\cdot\mathbf{r}\right)$
\end_inset

 which gives
\begin_inset Formula \[
\begin{array}{lclcl}
VR^{L}\chi_{-\kappa,m} & + & \frac{ic}{r}\left(\boldsymbol{\sigma}\cdot\mathbf{r}\right)\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right)R^{S}\chi_{-\kappa,m} & = & ER^{L}\chi_{-\kappa,m}\\
\frac{c}{r}\left(\boldsymbol{\sigma}\cdot\mathbf{r}\right)\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right)R^{L}\chi_{\kappa,m} & + & i\left(V-2mc^{2}\right)R^{S}\chi_{\kappa,m} & = & iER^{S}\chi_{\kappa,m}\end{array}\]

\end_inset

 Next we use the Dirac identity to obtain
\begin_inset Formula \begin{equation}
\left(\boldsymbol{\sigma}\cdot\mathbf{r}\right)\left(\boldsymbol{\sigma}\cdot\mathbf{p}\right)=-i(\mathbf{r}\cdot\boldsymbol{\nabla})+i\boldsymbol{\sigma}\cdot(\mathbf{r}\times\mathbf{p})=-i\left[r\frac{\partial}{\partial r}+(1+\widehat{\kappa})\right];\quad\widehat{\kappa}=-\left[\left(\boldsymbol{\sigma}\cdot\mathbf{l}\right)+1\right]\label{eq:sdr_sdp}\end{equation}

\end_inset

 which finally gives the radial Dirac equations
\begin_inset Formula \begin{equation}
\begin{array}{lclcl}
VR^{L} & + & \frac{c}{r}\left[r\frac{\partial}{\partial r}+(1-\kappa^{L})\right]R^{S} & = & ER^{L}\\
-\frac{c}{r}\left[r\frac{\partial}{\partial r}+(1+\kappa^{L})\right]R^{L} & + & \left(V-2mc^{2}\right)R^{S} & = & ER^{S}\end{array}\label{eq:radialeq}\end{equation}

\end_inset

The relation between the large and small component radial functions are
\begin_inset Formula \begin{equation}
cR^{S}=-\frac{1}{2mr}B(E)\left[r\frac{\partial}{\partial r}+(1+\kappa^{L})\right]R^{L};\quad B(E)=\left[1+\frac{E-V}{2mc^{2}}\right]^{-1}\label{eq:exact coupling}\end{equation}

\end_inset

 The kinetic balance relation is the non-relativistic limit of (
\begin_inset LatexCommand \ref{eq:exact coupling}

\end_inset

) in which 
\begin_inset Formula $B(E)$
\end_inset

 goes to one.
\end_layout

\begin_layout Subsection*
Basis functions
\end_layout

\begin_layout Standard
We now consider the realisation of this in various basis functions.
 
\emph on
Cartesian Gaussians
\emph default
 are defined as
\begin_inset Formula \begin{equation}
G_{ijk}^{\alpha}=G_{i}^{\alpha}(x)G_{j}^{\alpha}(y)G_{k}^{\alpha}(z)\label{eq:Cartesian Gaussian}\end{equation}

\end_inset

 with 
\begin_inset Formula \[
G_{i}^{\alpha}(x)=\left(\frac{2\alpha}{\pi}\right)^{1/4}\sqrt{\frac{\left(4\alpha\right)^{i}}{(2i-1)!!}}x^{i}\exp\left[-\alpha x^{2}\right]\]

\end_inset

Alternatively one may express a Cartesian Gaussian as
\begin_inset Formula \begin{equation}
G_{ijk}^{\alpha}=N_{ijk}^{\alpha}x^{i}y^{j}z^{k}\exp\left[-\alpha r^{2}\right];\quad N_{ijk}=\left(\frac{2\alpha}{\pi}\right)^{3/4}\sqrt{\frac{2^{l}}{F_{ijk}}}\left(\sqrt{2\alpha}\right)^{l}\label{eq:cart2}\end{equation}

\end_inset

The factor 
\begin_inset Formula \[
F_{ijk}=(2i-1)!!(2j-1)!!(2k-1)!!\]

\end_inset

 shows that the Cartesian components of a given shell may have different
 normalization constants.
 In HERMIT a single normalization is chosen by ignoring 
\begin_inset Formula $F_{ijk}$
\end_inset

 meaning that Cartesian Gaussians are normalized to
\begin_inset Formula \begin{equation}
\left\langle G_{ijk}^{\alpha}\left|\right.G_{ijk}^{\alpha}\right\rangle =F_{ijk}\label{eq:normcart}\end{equation}

\end_inset

In practice this means that app s- and p-functions are normalized to one.
 So are 
\begin_inset Formula $d110$
\end_inset

, 
\begin_inset Formula $d101$
\end_inset

 and 
\begin_inset Formula $d011$
\end_inset

, whereas 
\begin_inset Formula $d200$
\end_inset

, 
\begin_inset Formula $d020$
\end_inset

 and 
\begin_inset Formula $d002$
\end_inset

 are normalized to three.
 
\begin_inset Formula $f111$
\end_inset

 is normalized to one, 
\begin_inset Formula $f210$
\end_inset

, 
\begin_inset Formula $f201$
\end_inset

, 
\begin_inset Formula $f120$
\end_inset

, 
\begin_inset Formula $f102$
\end_inset

 and 
\begin_inset Formula $f012$
\end_inset

 are normalized to three, and 
\begin_inset Formula $f300$
\end_inset

, 
\begin_inset Formula $f030$
\end_inset

 and 
\begin_inset Formula $f003$
\end_inset

 are normalized to 15.
\end_layout

\begin_layout Standard

\emph on
Spherical Gaussians
\emph default
 are defined by
\begin_inset Formula \begin{equation}
G_{lm}^{\alpha}=R_{l}^{\alpha}(r)Y_{lm}(\theta,\phi);\quad R_{l}^{\alpha}=N_{l}^{\alpha}r^{l}\exp\left[-\alpha r^{2}\right];\quad N_{l}^{\alpha}=\frac{2(2\alpha)^{3/4}}{\pi^{1/4}}\sqrt{\frac{2^{l}}{(2l+1)!!}}\left(\sqrt{2\alpha}\right)^{l}\label{eq:spherical Gaussians}\end{equation}

\end_inset

 We note that
\begin_inset Formula \begin{equation}
N_{l}^{\alpha}=2\sqrt{\frac{\alpha}{2l+1}}N_{l-1}^{\alpha}\label{eq:radial normalization}\end{equation}

\end_inset

 
\emph on
Solid-harmonic Gaussians
\emph default
 are obtained as real-valued combinations of spherical Gaussians and may
 be written as
\begin_inset Formula \begin{equation}
G_{lm}^{\alpha}=S_{lm}\exp\left[-\alpha r^{2}\right]\label{eq:solid-harmonic Gaussians}\end{equation}

\end_inset

 Hermite Gaussians are defined as
\begin_inset Formula \begin{equation}
H_{ijk}^{\alpha}=H_{i}^{\alpha}(x)H_{j}^{\alpha}(y)H_{k}^{\alpha}(z)\label{eq:hermite Gaussian}\end{equation}

\end_inset

 with (add normalization !)
\begin_inset Formula \[
H_{i}^{\alpha}(x)=\frac{1}{(2\alpha)^{i}}\frac{d^{i}}{dA_{x}^{i}}\exp\left[-\alpha x^{2}\right]\]

\end_inset

 The Hermite Gaussians obey the recurrence relations
\begin_inset Formula \begin{equation}
H_{i+1}=xH_{i}-\frac{i}{2\alpha}H_{i-1}\label{eq:Hermite recurrence}\end{equation}

\end_inset


\end_layout

\begin_layout Subsection*
Kinetic balance
\end_layout

\begin_layout Standard
Kinetic balance sets 
\begin_inset Formula $B(E)=1$
\end_inset

.
 Starting from the radial function of a large component spherical Gaussian
 basis function one then obtains
\begin_inset Formula \begin{eqnarray}
R^{S} & \propto & -\left[\frac{\partial}{\partial r}+\frac{(1+\kappa^{L})}{r}\right]R_{l}^{\alpha}\nonumber \\
 & = & 2\alpha N_{l}^{\alpha}r^{l+1}\exp\left[-\alpha r^{2}\right]-N_{l}^{\alpha}(l+1+\kappa^{L})r^{l-1}\exp\left[-\alpha r^{2}\right]\nonumber \\
 & = & \sqrt{\alpha(2l+3)}R_{l+1}^{\alpha}-2\sqrt{\frac{\alpha}{2l+1}}(l+1+\kappa^{L})R_{l-1}^{\alpha}\label{eq:kinetic balance}\end{eqnarray}

\end_inset

We can now distinguish two cases
\begin_inset Formula \begin{equation}
\begin{array}{lclclcccl}
\kappa^{L} & = & l; &  & R^{S} & \propto & \sqrt{(2l+3)}R_{l+1}^{\alpha}-2\sqrt{(2l+1)}R_{l-1}^{\alpha} & \propto & \left[\frac{2\alpha r^{2}}{2l+1}-1\right]R_{l-1}^{\alpha}\\
\\\kappa^{L} & = & -(l+1); &  & R^{S} & \propto & R_{l+1}^{\alpha}\end{array}\label{eq:two cases}\end{equation}

\end_inset

 The case 
\begin_inset Formula $\kappa<0$
\end_inset

 is straightforward, but a bit more care is required for the implementation
 of the case 
\begin_inset Formula $\kappa>0$
\end_inset

.
 The modified spherical (or solid-harmonic) Gaussian to be constructed is
\begin_inset Formula \begin{equation}
G_{lm}^{*}=N\left\{ \sqrt{(2l+1)(2l-1)}R_{l}^{\alpha}-2(2l-1)R_{l-2}^{\alpha}\right\} Y_{l-2,m};\quad N=\frac{1}{\sqrt{(2l+1)(2l-1)}}\label{eq:modified sGTO}\end{equation}

\end_inset

We write the spherical harmonic 
\begin_inset Formula $G_{l-2,m}^{\alpha}$
\end_inset

 as a linear combination of Cartesian Gaussians
\begin_inset Formula \[
G_{l-2,m}^{\alpha}=\sum_{i+j+k=l-2}c_{ijk}^{l-2,m}G_{ijk}^{\alpha}\]

\end_inset

 In HERMIT we have selected a transformation such that the solid harmonics
 are normalized to unity.
 From this we obtain
\begin_inset Formula \begin{eqnarray}
G_{lm}^{*} & = & N\left[4\alpha r^{2}-2(2l-1)\right]\sum_{i+j+k=l-2}c_{ijk}^{l-2,m}G_{ijk}^{\alpha}=\nonumber \\
 & =N & \sum_{i+j+k=l-2}c_{ijk}^{l-2,m}\left[4\alpha\left(\frac{N_{ijk}^{\alpha}}{N_{i+2,j,k}^{\alpha}}G_{i+2,j,k}^{\alpha}+\frac{N_{ijk}^{\alpha}}{N_{i,j+2,k}^{\alpha}}G_{i,j+2,k}^{\alpha}+\frac{N_{ijk}^{\alpha}}{N_{i,j,k+2}^{\alpha}}G_{i,j,k+2}^{\alpha}\right)-2(2l-1)G_{ijk}^{\alpha}\right]\label{eq:Cartesian intermediate}\end{eqnarray}

\end_inset

 From (
\begin_inset LatexCommand \ref{eq:cart2}

\end_inset

) we see e.g.
 that
\begin_inset Formula \begin{equation}
\frac{N_{ijk}^{\alpha}}{N_{i+2,j,k}^{\alpha}}=\frac{1}{4\alpha}\sqrt{\frac{F_{i+2,j,k}}{F_{ijk}}}\label{eq:normratio}\end{equation}

\end_inset

 However, the expression is much simplified by the fact that factors 
\begin_inset Formula $F_{ijk}$
\end_inset

 are set to one in HERMIT, such that
\begin_inset Formula \begin{equation}
G_{lm}^{*}=N\sum_{i+j+k=l-2}c_{ijk}^{l-2,m}\left[\left(G_{i+2,j,k}^{\alpha}+G_{i,j+2,k}^{\alpha}+G_{i,j,k+2}^{\alpha}\right)-2(2l-1)G_{ijk}^{\alpha}\right]\label{eq:Cartesian modified}\end{equation}

\end_inset

 We now consider the expression of the modified spherical harmonics in Hermite
 Gaussians.
 We first consider the quantity
\begin_inset Formula \begin{equation}
H_{lm}^{*}=\sum_{ijk=l-2}c_{l-2,m}^{ijk}\left\{ H_{i+2,j,k}+H_{i,j+2,k}+H_{i,j,k+2}\right\} =\left[r^{2}-\frac{2l-1}{2\alpha}\right]\sum_{ijk=l-2}c_{l-2,m}^{ijk}H_{ijk}^{\alpha}+O(l-4)\label{eq:intermediate Hermite}\end{equation}

\end_inset

where the second term is obtained by repeated use of the recurrence relation
 (
\begin_inset LatexCommand \ref{eq:Hermite recurrence}

\end_inset

) 
\begin_inset Formula \[
H_{i+2}=\left(x^{2}-\frac{2i+1}{\alpha}\right)H_{i}-\frac{i(i-1)}{(2\alpha)^{2}}H_{i-2}\]

\end_inset

 Comparing (
\begin_inset LatexCommand \ref{eq:Cartesian intermediate}

\end_inset

) and (
\begin_inset LatexCommand \ref{eq:intermediate Hermite}

\end_inset

) shows that 
\begin_inset Formula \[
G_{lm}^{*}=N4\alpha H_{lm}^{*}\]

\end_inset

 A simple example may illustrate this connection.
 We consider
\begin_inset Formula \[
H_{20}^{\alpha}=H_{200}+H_{020}+H_{002}\]

\end_inset

 where we exploit that the spherical transformation of 
\begin_inset Formula $s$
\end_inset

 orbitals is simply unity.
 We next find
\begin_inset Formula \begin{eqnarray*}
H_{0}^{\alpha}(x) & = & \exp\left[-\alpha x^{2}\right]\\
H_{1}^{\alpha}(x) & = & x\exp\left[-\alpha x^{2}\right]\\
H_{2}(x) & = & (x^{2}-\frac{1}{2\alpha})\exp\left[-\alpha x^{2}\right]=xH_{1}-\frac{1}{2\alpha}H_{0}=(x^{2}-\frac{1}{2\alpha})H_{0}\end{eqnarray*}

\end_inset

which then gives
\begin_inset Formula \[
H_{20}^{\alpha*}=(r^{2}-\frac{1}{2\alpha})\exp\left[-\alpha r^{2}\right]\]

\end_inset

 to be compared with
\begin_inset Formula \[
G_{20}^{*\alpha}=\frac{4\alpha}{\sqrt{15}}(r^{2}-\frac{1}{2\alpha})\exp\left[-\alpha r^{2}\right]\]

\end_inset


\end_layout

\end_body
\end_document
