#!/usr/bin/env python

import os
import sys
import tarfile
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from runtest_dirac import Filter, TestRun

test = TestRun(__file__, sys.argv)

f = Filter()
f.add(string = '**** Inverse mean delocalization',
      rel_tolerance = 1.0e-7)

# prepare reference orbitals
test.run(['H.inp'], ['H.xyz'], args='--outcmo')
if os.path.isfile('CHECKPOINT.h5'):
    ext = '.h5'
elif os.path.isfile('CHECKPOINT.noh5.tar.gz'):
    ext = '.noh5.tar.gz'
shutil.move('CHECKPOINT'+ext, 'H'+ext)
test.run(['C.inp'], ['C.xyz'], args='--outcmo')
shutil.move('CHECKPOINT'+ext, 'C'+ext)
test.run(['O.inp'], ['O.xyz'], args='--outcmo')
shutil.move('CHECKPOINT'+ext, 'O'+ext)

shutil.copy('H'+ext, 'H1'+ext)
shutil.copy('H'+ext, 'H2'+ext)
shutil.copy('H'+ext, 'H3'+ext)
shutil.copy('H'+ext, 'H4'+ext)
shutil.copy('C'+ext, 'C1'+ext)
shutil.copy('O'+ext, 'O1'+ext)

# CH4
shutil.copy('cf.CH4', 'DFPCMO')
test.run(['CH4locmul_full.inp'], ['CH4.mol'], f, '--copy="DFPCMO"')
test.run(['CH4locprj_full.inp'], ['CH4.mol'], f, '--copy="DFPCMO C1.h5 H1.h5 H2.h5 H3.h5 H4.h5"')
test.run(['CH4locprj_comb.inp'], ['CH4.mol'], f, '--copy="DFPCMO C1.h5 H1.h5 H2.h5 H3.h5 H4.h5"')
test.run(['CH4locprj_diag.inp'], ['CH4.mol'], f, '--copy="DFPCMO C1.h5 H1.h5 H2.h5 H3.h5 H4.h5"')

# H2O
shutil.copy('cf.H2O', 'DFPCMO')
test.run(['H2Olocmul_full.inp'], ['H2O.mol'], f, '--copy="DFPCMO"')
test.run(['H2Olocprj_full.inp'], ['H2O.mol'], f, '--copy="DFPCMO O1.h5 H1.h5 H2.h5"')
test.run(['H2Olocprj_comb.inp'], ['H2O.mol'], f, '--copy="DFPCMO O1.h5 H1.h5 H2.h5"')
test.run(['H2Olocprj_diag.inp'], ['H2O.mol'], f, '--copy="DFPCMO O1.h5 H1.h5 H2.h5"')

# cleanup
os.unlink('C1'+ext)
os.unlink('O1'+ext)
os.unlink('H1'+ext)
os.unlink('H2'+ext)
os.unlink('H3'+ext)
os.unlink('H4'+ext)
os.unlink('DFPCMO')

sys.exit(test.return_code)
