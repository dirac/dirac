! FILE: cb_bitdef.h
!     Written by H. J. Aa. Jensen - July 2016
!     Initialized by ini_cb_bittab
      integer bit_cnt, bit_dif
      common /cb_bitdef/ bit_cnt(0:255), bit_dif(0:255,0:255)
! .. end of cb_bitdef.h ..
